import pygame
import sys
from automaton import Automaton
from typing import Tuple


class MapCell:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y
        self.light_intensity = 0
        self.objects = []


class LightSource:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y
        self.max_intensity = 255
        self.max_distance = 50

    def calculate_light_intensity(self, position: Tuple[int, int]) -> int:
        distance = ((self.x - position[0]) ** 2 + (self.y - position[1]) ** 2) ** 0.5

        if distance > self.max_distance:
            return 0

        intensity = self.max_intensity * (1 - (distance / self.max_distance))
        return int(intensity)


class WorldMap:
    def __init__(self, size: int, light_source: LightSource):
        self.size = size
        self.cells = [[MapCell(x, y) for y in range(size)] for x in range(size)]
        self.light_source = light_source

    def update_light(self):
        for row in self.cells:
            for cell in row:
                cell.light_intensity = self.light_source.calculate_light_intensity((cell.x, cell.y))

    def get_light_intensity(self, x: int, y: int) -> int:
        if 0 <= x < self.size and 0 <= y < self.size:
            return self.cells[x][y].light_intensity
        return 0


# Инициализация Pygame
pygame.init()

# Константы для отображения
SCREEN_SIZE = 600
CELL_SIZE = SCREEN_SIZE // 100

# Создание карты и источника света
light_source = LightSource(50, 50)
world_map = WorldMap(100, light_source)
world_map.update_light()

# Создание автоматона
automaton = Automaton((50, 50))

# Создание окна
screen = pygame.display.set_mode((SCREEN_SIZE, SCREEN_SIZE))
pygame.display.set_caption("Light Field Visualization")

# Шрифты для текста
font = pygame.font.SysFont(None, 24)


def draw_text(surface, text, pos, color=(255, 255, 255)):
    text_surface = font.render(text, True, color)
    surface.blit(text_surface, pos)


def draw_automaton(surface, automaton: Automaton):
    x, y = automaton.x * CELL_SIZE, automaton.y * CELL_SIZE
    direction = automaton.direction

    if direction == 0:  # Вверх
        points = [(x + CELL_SIZE // 2, y), (x, y + CELL_SIZE), (x + CELL_SIZE, y + CELL_SIZE)]
    elif direction == 1:  # Вправо
        points = [(x + CELL_SIZE, y + CELL_SIZE // 2), (x, y), (x, y + CELL_SIZE)]
    elif direction == 2:  # Вниз
        points = [(x + CELL_SIZE // 2, y + CELL_SIZE), (x, y), (x + CELL_SIZE, y)]
    elif direction == 3:  # Влево
        points = [(x, y + CELL_SIZE // 2), (x + CELL_SIZE, y), (x + CELL_SIZE, y + CELL_SIZE)]

    pygame.draw.polygon(surface, (255, 0, 0), points)


# Основной цикл
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False
            elif event.key == pygame.K_w:
                automaton.move(0, -1)
            elif event.key == pygame.K_s:
                automaton.move(0, 1)
            elif event.key == pygame.K_a:
                automaton.move(-1, 0)
            elif event.key == pygame.K_d:
                automaton.move(1, 0)
            elif event.key == pygame.K_q:
                automaton.rotate_left()
            elif event.key == pygame.K_e:
                automaton.rotate_right()

    automaton.update_sensors(world_map)

    screen.fill((0, 0, 0))

    for row in world_map.cells:
        for cell in row:
            color = (cell.light_intensity, cell.light_intensity, cell.light_intensity)
            pygame.draw.rect(screen, color, (cell.x * CELL_SIZE, cell.y * CELL_SIZE, CELL_SIZE, CELL_SIZE))

    draw_automaton(screen, automaton)
    draw_text(screen, f"Left: {automaton.left_sensor}", (10, 10))
    draw_text(screen, f"Front: {automaton.front_sensor}", (10, 30))
    draw_text(screen, f"Right: {automaton.right_sensor}", (10, 50))

    pygame.display.flip()

pygame.quit()
sys.exit()
