from typing import Tuple

class Automaton:
    DIRECTIONS = [(0, -1), (1, 0), (0, 1), (-1, 0)]  # Вверх, вправо, вниз, влево

    def __init__(self, initial_cell: Tuple[int, int], direction: int = 0):
        self.x, self.y = initial_cell
        self.direction = direction  # 0 - вверх, 1 - вправо, 2 - вниз, 3 - влево
        self.left_sensor = 0
        self.front_sensor = 0
        self.right_sensor = 0

    def rotate_left(self):
        self.direction = (self.direction - 1) % 4

    def rotate_right(self):
        self.direction = (self.direction + 1) % 4

    def move(self, dx: int, dy: int):
        self.x += dx
        self.y += dy

    def update_sensors(self, world_map):
        dx, dy = self.DIRECTIONS[self.direction]
        front_x, front_y = self.x + dx, self.y + dy

        left_dir = (self.direction - 1) % 4
        dx_left, dy_left = self.DIRECTIONS[left_dir]
        left_x, left_y = self.x + dx_left, self.y + dy_left

        right_dir = (self.direction + 1) % 4
        dx_right, dy_right = self.DIRECTIONS[right_dir]
        right_x, right_y = self.x + dx_right, self.y + dy_right

        self.front_sensor = world_map.get_light_intensity(front_x, front_y)
        self.left_sensor = world_map.get_light_intensity(left_x, left_y)
        self.right_sensor = world_map.get_light_intensity(right_x, right_y)
